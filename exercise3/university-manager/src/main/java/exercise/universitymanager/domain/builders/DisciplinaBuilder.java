package exercise.universitymanager.domain.builders;

import exercise.universitymanager.domain.dtos.CursoDTO;
import exercise.universitymanager.domain.dtos.DisciplinaDTO;
import exercise.universitymanager.domain.inputs.CursoInput;
import exercise.universitymanager.domain.inputs.DisciplinaInput;
import exercise.universitymanager.domain.models.Curso;
import exercise.universitymanager.domain.models.Disciplina;

public class DisciplinaBuilder {


    public static DisciplinaDTO build(Disciplina disciplina) {
        return DisciplinaDTO.builder()
                .nomeDisciplina(disciplina.getNomeDisciplina())
                .descricao(disciplina.getDescricao())
                .build();
    }

    public static Disciplina build(DisciplinaInput input) {
        return Disciplina.builder()
                .nomeDisciplina(input.getNomeDisciplina())
                .codigoDisciplina(input.getCodigoDisciplina())
                .descricao(input.getDescricao())
                .build();
    }
}
