package exercise.universitymanager.domain.inputs;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@Schema(description = "Representação de entrada de novo professor.") // Quais dados alguém precisa preencher pra criar um professor
public class ProfessorInput {

    @NotBlank(message = "CPF do professor não pode ser vazio!")
    @Size(max = 11)
    private String cpf;

    @NotBlank(message = "Nome do professor não pode ser vazio!")
    private String nome;

    @NotBlank(message = "Titulação do professor não pode ser vazia!")
    private String titulacao;
}

