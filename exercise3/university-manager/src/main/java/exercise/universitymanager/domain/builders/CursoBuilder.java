package exercise.universitymanager.domain.builders;


import exercise.universitymanager.domain.dtos.CursoDTO;
import exercise.universitymanager.domain.inputs.CursoInput;
import exercise.universitymanager.domain.models.Aluno;
import exercise.universitymanager.domain.models.Curso;

public class CursoBuilder {

    public static CursoDTO build(Curso curso) {
        return CursoDTO.builder()
                .nome(curso.getNome())
                .nomePredio(curso.getNomePredio())
                .coordenadorId(curso.getCoordenador())
                .build();
    }

    public static Curso build(CursoInput input) {
        return Curso.builder()
                .nome(input.getNome())
                .nomePredio(input.getNomePredio())
                .coordenador(input.getCoordenadorId())
                .dataCriacao(input.getDataCriacao())
                .build();
    }
}
