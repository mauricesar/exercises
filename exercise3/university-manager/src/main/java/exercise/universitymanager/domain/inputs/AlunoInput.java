package exercise.universitymanager.domain.inputs;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Schema(description = "Representação de entrada de novo estudante.") // Quais dados alguém precisa preencher pra criar um estudante
public class AlunoInput {
    @NotBlank(message = "CPF não pode ser vazio!")
    @Size(min = 11, max = 11)
    private String cpf;

    @NotBlank(message = "Nome precisa ser preenchido!")
    @Size(max = 100)
    private String nome;

    @NotNull(message = "Data de nascimento do aluno não pode ser vazia!")
    @Past
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate dataNascimento;

    @NotBlank(message = "RG precisa ser preenchido!")
    @Size(max = 20)
    private String rg;

    @NotBlank(message = "Órgão expedidor precisa ser preenchido!")
    @Size(max = 10)
    private String orgaoExpedidor;
}

