package exercise.universitymanager.domain.builders;

import exercise.universitymanager.domain.dtos.AlunoDTO;
import exercise.universitymanager.domain.inputs.AlunoInput;
import exercise.universitymanager.domain.models.Aluno;

public class AlunoBuilder {


    public static AlunoDTO build(Aluno aluno) {
        return AlunoDTO.builder()
                .id(aluno.getId())
                .cpf(aluno.getCpf())
                .nome(aluno.getNome())
                .rg(aluno.getRg())
                .build();
    }

    public static Aluno build(AlunoInput input) {
        return Aluno.builder()
                .cpf(input.getCpf())
                .nome(input.getNome())
                .dataNascimento(input.getDataNascimento())
                .rg(input.getRg())
                .orgaoExpedidor(input.getOrgaoExpedidor())
                .build();
    }

}
