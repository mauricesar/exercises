package exercise.universitymanager.domain.builders;

import exercise.universitymanager.domain.dtos.DisciplinaDTO;
import exercise.universitymanager.domain.dtos.ProfessorDTO;
import exercise.universitymanager.domain.inputs.DisciplinaInput;
import exercise.universitymanager.domain.inputs.ProfessorInput;
import exercise.universitymanager.domain.models.Disciplina;
import exercise.universitymanager.domain.models.Professor;

public class ProfessorBuilder {

    public static ProfessorDTO build(Professor professor) {
        return ProfessorDTO.builder()
                .nome(professor.getNome())
                .cpf(professor.getCpf())
                .build();
    }

    public static Professor build(ProfessorInput input) {
        return Professor.builder()
                .nome(input.getNome())
                .cpf(input.getCpf())
                .titulacao(input.getTitulacao())
                .build();
    }
}
