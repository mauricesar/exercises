package exercise.universitymanager.domain.inputs;


import exercise.universitymanager.domain.models.Professor;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Schema(description = "Representação de entrada de novo curso.") // Quais dados alguém precisa preencher pra criar um curso
public class CursoInput {
    @NotBlank(message = "Nome do curso não pode ser vazio!")
    private String nome;

    @NotNull(message = "Data de criação do curso não pode ser vazia!")
    private LocalDate dataCriacao;

    @NotBlank(message = "Nome do prédio do curso não pode ser vazio!")
    private String nomePredio;

    @NotNull(message = "ID do coordenador não pode ser vazio!")
    private Professor coordenadorId;
}
