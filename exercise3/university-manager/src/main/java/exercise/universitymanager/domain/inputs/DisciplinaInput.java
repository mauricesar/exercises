package exercise.universitymanager.domain.inputs;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
@Schema(description = "Representação de entrada de uma nova disciplina.") // Quais dados alguém precisa preencher pra criar uma disciplina
public class DisciplinaInput {

    @NotBlank(message = "Nome da disciplina não pode ser vazio!")
    private String nomeDisciplina;

    @NotBlank(message = "Código da disciplina não pode ser vazio!")
    private String codigoDisciplina;

    @NotBlank(message = "Descrição da disciplina não pode ser vazia!")
    private String descricao;
}

