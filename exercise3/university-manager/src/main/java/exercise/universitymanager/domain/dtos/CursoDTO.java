package exercise.universitymanager.domain.dtos;

import exercise.universitymanager.domain.models.Professor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CursoDTO {
    private String nome;
    private String nomePredio;
    private Professor coordenadorId;
}
