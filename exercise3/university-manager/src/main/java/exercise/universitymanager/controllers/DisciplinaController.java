package exercise.universitymanager.controllers;

import exercise.universitymanager.domain.builders.DisciplinaBuilder;
import exercise.universitymanager.domain.dtos.DisciplinaDTO;
import exercise.universitymanager.domain.inputs.DisciplinaInput;
import exercise.universitymanager.domain.models.Disciplina;
import exercise.universitymanager.services.DisciplinaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/disciplinas")
public class DisciplinaController {

    private final DisciplinaService disciplinaService;

    public DisciplinaController(DisciplinaService disciplinaService) {
        this.disciplinaService = disciplinaService;
    }

    @PostMapping
    public ResponseEntity<DisciplinaDTO> cadastrarDisciplina(@RequestBody DisciplinaInput disciplinaInput) {
        try {
            DisciplinaDTO novaDisciplina = disciplinaService.cadastrarDisciplina(disciplinaInput);
            return ResponseEntity.ok(novaDisciplina);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
