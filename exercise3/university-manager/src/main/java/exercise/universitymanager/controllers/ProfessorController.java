package exercise.universitymanager.controllers;

import exercise.universitymanager.domain.dtos.ProfessorDTO;
import exercise.universitymanager.domain.inputs.ProfessorInput;
import exercise.universitymanager.domain.models.Professor;
import exercise.universitymanager.services.ProfessorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/professores")
public class ProfessorController {
    private final ProfessorService professorService;

    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @PostMapping
    public ResponseEntity<ProfessorDTO> cadastrarProfessor(@RequestBody @Valid ProfessorInput professorInput) {
        try {
            ProfessorDTO novoProfessor = professorService.cadastrarProfessor(professorInput);
            return ResponseEntity.ok(novoProfessor);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
