package exercise.universitymanager.controllers;

import exercise.universitymanager.domain.dtos.AlunoDTO;
import exercise.universitymanager.domain.inputs.AlunoInput;
import exercise.universitymanager.services.AlunoService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/alunos")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AlunoController {

    private final AlunoService alunoService;
    @PostMapping("/cadastrar")
    public AlunoDTO cadastrarAluno(@RequestBody @Valid AlunoInput alunoInput){
        return alunoService.cadastrarAluno(alunoInput);
    }


}
