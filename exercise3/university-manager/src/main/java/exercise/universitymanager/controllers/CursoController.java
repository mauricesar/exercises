package exercise.universitymanager.controllers;

import exercise.universitymanager.domain.dtos.CursoDTO;
import exercise.universitymanager.domain.inputs.CursoInput;
import exercise.universitymanager.services.CursoService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/cursos")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CursoController {

    private final CursoService cursoService;
    @PostMapping
    public ResponseEntity<CursoDTO> cadastrarCurso(@Valid @RequestBody CursoInput cursoInput) {
        CursoDTO novoCurso = cursoService.cadastrarCurso(cursoInput);
        return ResponseEntity.status(HttpStatus.CREATED).body(novoCurso);
    }
}
