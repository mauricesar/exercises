package exercise.universitymanager.exceptions;

public class ExceptionHandler {

    public static class CadastroException extends RuntimeException {
        public CadastroException(String message) {
            super(message);
        }
    }

    public static class CadastroCursoException extends CadastroException {
        public CadastroCursoException(String message) {
            super("Erro ao cadastrar o curso: " + message);
        }
    }

    public static class CadastroDisciplinaException extends CadastroException {
        public CadastroDisciplinaException(String message) {
            super("Erro ao cadastrar disciplina: " + message);
        }
    }

    public static class CadastroProfessorException extends CadastroException {
        public CadastroProfessorException(String message) {
            super("Erro ao cadastrar o professor: " + message);
        }
    }

    public static class CadastroAlunoException extends CadastroException {
        public CadastroAlunoException(String message) {
            super("Erro ao cadastrar o aluno: " + message);
        }
    }
}
