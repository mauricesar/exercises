package exercise.universitymanager.services;

import exercise.universitymanager.domain.builders.AlunoBuilder;
import exercise.universitymanager.domain.dtos.AlunoDTO;
import exercise.universitymanager.domain.inputs.AlunoInput;
import exercise.universitymanager.domain.models.Aluno;
import exercise.universitymanager.exceptions.ExceptionHandler.CadastroAlunoException;
import exercise.universitymanager.repository.AlunoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.validation.Valid;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AlunoService {

    private final AlunoRepository alunoRepository;

    public AlunoDTO cadastrarAluno(@Valid AlunoInput alunoInput) throws CadastroAlunoException {
        try {
            Aluno aluno = AlunoBuilder.build(alunoInput);
            alunoRepository.save(aluno);
            return AlunoBuilder.build(aluno);
        } catch (Exception e) {
            throw new CadastroAlunoException(e.getMessage());
        }
    }
}
