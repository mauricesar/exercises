package exercise.universitymanager.services;

import exercise.universitymanager.domain.builders.ProfessorBuilder;
import exercise.universitymanager.domain.dtos.ProfessorDTO;
import exercise.universitymanager.domain.inputs.ProfessorInput;
import exercise.universitymanager.domain.models.Professor;
import exercise.universitymanager.exceptions.ExceptionHandler.CadastroProfessorException;
import exercise.universitymanager.repository.ProfessorRepository;
import org.springframework.stereotype.Service;

@Service
public class ProfessorService {
    private final ProfessorRepository professorRepository;

    public ProfessorService(ProfessorRepository professorRepository) {
        this.professorRepository = professorRepository;
    }

    public ProfessorDTO cadastrarProfessor(ProfessorInput professorInput) throws CadastroProfessorException {
        try {
            Professor professor = ProfessorBuilder.build(professorInput);
            Professor novoProfessor = professorRepository.save(professor);
            return ProfessorBuilder.build(novoProfessor);
        } catch (Exception e) {
            throw new CadastroProfessorException(e.getMessage());
        }
    }
}
