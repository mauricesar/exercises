package exercise.universitymanager.services;

import exercise.universitymanager.domain.builders.DisciplinaBuilder;
import exercise.universitymanager.domain.dtos.DisciplinaDTO;
import exercise.universitymanager.domain.inputs.DisciplinaInput;
import exercise.universitymanager.domain.models.Disciplina;
import exercise.universitymanager.exceptions.ExceptionHandler.CadastroDisciplinaException;
import exercise.universitymanager.repository.DisciplinaRepository;
import org.springframework.stereotype.Service;

@Service
public class DisciplinaService {

    private final DisciplinaRepository disciplinaRepository;

    public DisciplinaService(DisciplinaRepository disciplinaRepository) {
        this.disciplinaRepository = disciplinaRepository;
    }

    public DisciplinaDTO cadastrarDisciplina(DisciplinaInput disciplinaInput) throws CadastroDisciplinaException {
        Disciplina disciplina = DisciplinaBuilder.build(disciplinaInput);

        try {
            disciplina = disciplinaRepository.save(disciplina);
            return DisciplinaBuilder.build(disciplina);
        } catch (Exception e) {
            throw new CadastroDisciplinaException(e.getMessage());
        }
    }
}
