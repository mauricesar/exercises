package exercise.universitymanager.services;

import exercise.universitymanager.domain.dtos.CursoDTO;
import exercise.universitymanager.domain.inputs.CursoInput;
import exercise.universitymanager.domain.builders.CursoBuilder;
import exercise.universitymanager.exceptions.ExceptionHandler.CadastroCursoException;
import exercise.universitymanager.domain.models.Curso;
import exercise.universitymanager.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoService {

    private final CursoRepository cursoRepository;
    @Autowired
    public CursoService(CursoRepository cursoRepository) {
        this.cursoRepository = cursoRepository;
    }

    public CursoDTO cadastrarCurso(CursoInput cursoInput) throws CadastroCursoException {
        Curso curso = CursoBuilder.build(cursoInput);
        try {
            Curso cursoCadastrado = cursoRepository.save(curso);
            return CursoBuilder.build(cursoCadastrado);
        } catch (Exception e) {
            throw new CadastroCursoException(e.getMessage());
        }
    }
}
