package exercise2;

import java.sql.*;
import java.time.LocalDate;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.List;
import java.util.ArrayList;

public class Cadastro {
    private static List<Aluno> alunos = new ArrayList<>();
    private static List<Curso> cursos = new ArrayList<>();
    private static List<Disciplina> disciplinas = new ArrayList<>();
    private static List<Professor> professores = new ArrayList<>();
    public static List<Aluno> getAlunos() {
        return alunos;
    }
    public static List<Disciplina> getDisciplinas() {
        return disciplinas;
    }
    public static List<Professor> getProfessores() {
        return professores;
    }



    public static Aluno cadastrarAluno() {
        Scanner scanner = new Scanner(System.in);
        Connection connection = null;
        Aluno aluno = null;

        try {
            System.out.println("===== Cadastro de Aluno =====");
            System.out.print("CPF: ");
            String cpf = scanner.nextLine();

            System.out.print("Nome: ");
            String nome = scanner.nextLine();

            System.out.print("Data de Nascimento (dd-MM-yyyy): ");
            String dataNascimentoStr = scanner.nextLine();
            LocalDate dataNascimento = LocalDate.parse(dataNascimentoStr, DateTimeFormatter.ofPattern("dd-MM-yyyy"));

            System.out.print("RG: ");
            String rg = scanner.nextLine();

            System.out.print("Órgão Expedidor: ");
            String orgaoExpedidor = scanner.nextLine();

            // geração de ID
            String alunoId = UUID.randomUUID().toString();

            aluno = new Aluno(alunoId, cpf, nome, dataNascimento, rg, orgaoExpedidor);

            // associar o aluno a um curso
            System.out.print("Curso: ");
            String nomeCurso = scanner.nextLine();

            // criar um novo objeto Curso
            String cursoId = UUID.randomUUID().toString();
            Curso curso = new Curso(cursoId, nomeCurso, LocalDate.now(), "");

            // associar o curso ao aluno
            aluno.setCurso(curso);

            // inserir os dados no banco de dados
            try {
                connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");
                Statement statement = connection.createStatement();
                String sql = "INSERT INTO Alunos (id, cpf, nome, data_nascimento, rg, orgao_expedidor) " +
                        "VALUES (?, ?, ?, ?, ?, ?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, alunoId);
                preparedStatement.setString(2, cpf);
                preparedStatement.setString(3, nome);
                preparedStatement.setString(4, dataNascimento.format(DateTimeFormatter.ISO_LOCAL_DATE));
                preparedStatement.setString(5, rg);
                preparedStatement.setString(6, orgaoExpedidor);
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            // Fechar o scanner no bloco finally para garantir que seja sempre fechado
            if (scanner != null) {
                scanner.close();
            }

            // Fechar a conexão com o banco de dados no bloco finally
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return aluno;
    }



    public static Curso cadastrarCurso() throws Excecao.CadastroCursoException {
        Scanner scanner = new Scanner(System.in);
        Connection connection = null;
        Curso curso = null;

        try {
            System.out.println("===== Cadastro de Curso =====");
            System.out.print("Nome do Curso: ");
            String nome = scanner.nextLine();

            // geração de ID
            String cursoId = UUID.randomUUID().toString();

            curso = new Curso(cursoId, nome, LocalDate.now(), "");

            System.out.print("Nome do Prédio: ");
            String nomePredio = scanner.nextLine();
            curso.setNomePredio(nomePredio);

            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");
            Statement statement = connection.createStatement();
            String query = "INSERT INTO Cursos (id, nome, dataCriacao, nomePredio) VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, curso.getId());
            preparedStatement.setString(2, curso.getNome());
            preparedStatement.setString(3, curso.getDataCriacao().toString());
            preparedStatement.setString(4, curso.getNomePredio());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new Excecao.CadastroCursoException(e.getMessage());
        } finally {
            if (scanner != null) {
                scanner.close();
            }
            // fechar a conexão com o banco de dados
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return curso;
    }




    public static Disciplina cadastrarDisciplina() throws Excecao.CadastroDisciplinaException {
        Scanner scanner = new Scanner(System.in);
        Connection connection = null;
        Disciplina disciplina = null;

        try {
            System.out.println("===== Cadastro de Disciplina =====");
            System.out.print("Nome da Disciplina: ");
            String nome = scanner.nextLine();

            System.out.print("Código da Disciplina: ");
            String codigo = scanner.nextLine();

            System.out.print("Descrição da Disciplina: ");
            String descricao = scanner.nextLine();

            // Geração de ID
            String disciplinaId = UUID.randomUUID().toString();

            disciplina = new Disciplina(disciplinaId, nome, codigo, descricao);

            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");
            Statement statement = connection.createStatement();
            String query = "INSERT INTO Disciplinas (id, nomeDisciplina, codigoDisciplina, descricao) VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, disciplina.getDisciplinaId());
            preparedStatement.setString(2, disciplina.getNomeDisciplina());
            preparedStatement.setString(3, disciplina.getCodigoDisciplina());
            preparedStatement.setString(4, disciplina.getDescricao());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new Excecao.CadastroDisciplinaException(e.getMessage());
        } finally {
            if (scanner != null) {
                scanner.close();
            }
            // Fechar a conexão com o banco de dados
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return disciplina;
    }



    public static Professor cadastrarProfessor() throws Excecao.CadastroProfessorException {
        Scanner scanner = new Scanner(System.in);
        Connection connection = null;
        Professor professor = null;

        try {
            System.out.println("===== Cadastro de Professor =====");
            System.out.print("CPF: ");
            String cpf = scanner.nextLine();

            System.out.print("Nome: ");
            String nome = scanner.nextLine();

            // validar titulação
            String titulacao = "";
            while (true) {
                System.out.print("Titulação (Graduação, Mestrado, Doutorado): ");
                titulacao = scanner.nextLine();

                if (titulacao.equalsIgnoreCase("Graduação") ||
                        titulacao.equalsIgnoreCase("Mestrado") ||
                        titulacao.equalsIgnoreCase("Doutorado")) {
                    break;  // Sai do loop se a titulação for válida
                } else {
                    System.out.println("Opção inválida. Digite novamente.");
                }
            }

            // geração de ID
            String professorId = UUID.randomUUID().toString();

            professor = new Professor(professorId, cpf, nome, titulacao);

            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");
            Statement statement = connection.createStatement();
            String query = "INSERT INTO Professores (id, cpf, nome, titulacao) VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, professor.getProfessorId());
            preparedStatement.setString(2, professor.getCpf());
            preparedStatement.setString(3, professor.getNome());
            preparedStatement.setString(4, professor.getTitulacao());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new Excecao.CadastroProfessorException(e.getMessage());
        } finally {
            if (scanner != null) {
                scanner.close();
            }
            // fechar a conexão com o banco de dados
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return professor;
    }





    public static void alterarCursoAluno() throws Excecao.AlterarCursoAlunoException, Excecao.CursoNaoCadastradoException {
        Scanner scanner = new Scanner(System.in);
        Connection connection = null;

        try {
            System.out.println("===== Alterar Curso do Aluno =====");
            System.out.print("Nome do Aluno: ");
            String nomeAluno = scanner.nextLine();

            // verifica se existem alunos com o mesmo nome
            List<Aluno> alunosEncontrados = new ArrayList<>();
            for (Aluno aluno : alunos) {
                if (aluno.getNome().equalsIgnoreCase(nomeAluno)) {
                    alunosEncontrados.add(aluno);
                }
            }

            // se houver mais de um aluno com o mesmo nome, solicita o RG para verificação
            if (alunosEncontrados.size() > 1) {
                System.out.print("RG do Aluno: ");
                String rgAluno = scanner.nextLine();

                Aluno alunoEncontrado = null;
                for (Aluno aluno : alunosEncontrados) {
                    if (aluno.getRg().equalsIgnoreCase(rgAluno)) {
                        alunoEncontrado = aluno;
                        break;
                    }
                }

                if (alunoEncontrado == null) {
                    System.out.println("Aluno não encontrado.");
                    return;
                }

                System.out.println("Aluno encontrado:");
                System.out.println(alunoEncontrado);
                System.out.println();
            } else if (alunosEncontrados.size() == 1) {
                System.out.println("Aluno encontrado:");
                System.out.println(alunosEncontrados.get(0));
                System.out.println();
            } else {
                System.out.println("Aluno não encontrado.");
                return;
            }

            System.out.print("Nome do Novo Curso: ");
            String nomeCurso = scanner.nextLine();

            // procura o curso pelo nome
            Curso cursoEncontrado = null;
            for (Curso curso : cursos) {
                if (curso.getNome().equalsIgnoreCase(nomeCurso)) {
                    cursoEncontrado = curso;
                    break;
                }
            }

            // caso o curso não seja encontrado
            if (cursoEncontrado == null) {
                throw new Excecao.CursoNaoCadastradoException();
            }

            // alteração no curso do aluno
            Aluno aluno = alunosEncontrados.get(0);
            aluno.setCurso(cursoEncontrado);

            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");
            Statement statement = connection.createStatement();
            String query = "UPDATE Alunos SET cursoId = ? WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, cursoEncontrado.getId());
            preparedStatement.setString(2, aluno.getAlunoId());
            preparedStatement.executeUpdate();
            System.out.println("Curso do aluno alterado com sucesso.");
        } catch (SQLException e) {
            throw new Excecao.AlterarCursoAlunoException(e.getMessage());
        } finally {
            if (scanner != null) {
                scanner.close();
            }
            // fechar a conexão com o banco de dados
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }



    public static void matricularAlunoEmDisciplina(List<Aluno> alunos, List<Disciplina> disciplinas) throws Excecao.MatriculaAlunoDisciplinaException {
        Scanner scanner = new Scanner(System.in);
        Connection connection = null;
        Statement statement = null;

        try {
            System.out.print("Nome do Aluno: ");
            String nomeAluno = scanner.nextLine();

            List<Aluno> alunosEncontrados = new ArrayList<>();
            for (Aluno aluno : alunos) {
                if (aluno.getNome().equalsIgnoreCase(nomeAluno)) {
                    alunosEncontrados.add(aluno);
                }
            }

            if (alunosEncontrados.isEmpty()) {
                System.out.println("Aluno não encontrado.");
                return;
            }

            Aluno alunoEncontrado = null;
            if (alunosEncontrados.size() > 1) {
                System.out.print("RG do Aluno: ");
                String rgAluno = scanner.nextLine();

                for (Aluno aluno : alunosEncontrados) {
                    if (aluno.getRg().equalsIgnoreCase(rgAluno)) {
                        alunoEncontrado = aluno;
                        break;
                    }
                }

                if (alunoEncontrado == null) {
                    System.out.println("Aluno não encontrado.");
                    return;
                }
            } else {
                alunoEncontrado = alunosEncontrados.get(0);
            }

            System.out.println("Aluno encontrado:");
            System.out.println(alunoEncontrado);
            System.out.println();

            System.out.print("Nome da Disciplina: ");
            String nomeDisciplina = scanner.nextLine();

            List<Disciplina> disciplinasEncontradas = new ArrayList<>();
            for (Disciplina disciplina : disciplinas) {
                if (disciplina.getNomeDisciplina().equalsIgnoreCase(nomeDisciplina)) {
                    disciplinasEncontradas.add(disciplina);
                }
            }

            if (disciplinasEncontradas.isEmpty()) {
                System.out.println("Disciplina não encontrada.");
                return;
            }

            Disciplina disciplinaEncontrada = null;
            if (disciplinasEncontradas.size() > 1) {
                System.out.print("Código da Disciplina: ");
                String codigoDisciplina = scanner.nextLine();

                for (Disciplina disciplina : disciplinasEncontradas) {
                    if (disciplina.getCodigoDisciplina().equalsIgnoreCase(codigoDisciplina)) {
                        disciplinaEncontrada = disciplina;
                        break;
                    }
                }

                if (disciplinaEncontrada == null) {
                    System.out.println("Disciplina não encontrada.");
                    return;
                }
            } else {
                disciplinaEncontrada = disciplinasEncontradas.get(0);
            }

            System.out.println("Disciplina encontrada:");
            System.out.println(disciplinaEncontrada);
            System.out.println();

            String matriculaId = UUID.randomUUID().toString();
            String alunoId = alunoEncontrado.getAlunoId();
            String disciplinaId = disciplinaEncontrada.getDisciplinaId();

            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");
            statement = connection.createStatement();

            String query = "INSERT INTO Matriculas (matricula_id, aluno_id, disciplina_id) VALUES (?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, matriculaId);
            preparedStatement.setString(2, alunoId);
            preparedStatement.setString(3, disciplinaId);
            preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();

            alunoEncontrado.adicionarDisciplina(disciplinaEncontrada);
            disciplinaEncontrada.adicionarAluno(alunoEncontrado);

            System.out.println("Aluno matriculado na disciplina com sucesso!");
            System.out.println();
        } catch (SQLException e) {
            throw new Excecao.MatriculaAlunoDisciplinaException(e.getMessage());
        } finally {
            if (scanner != null) {
                scanner.close();
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }




    public static void removerMatriculaDisciplina(List<Aluno> alunos) throws Excecao.DisciplinaNaoExisteException {
        Scanner scanner = new Scanner(System.in);
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            System.out.print("Nome da Disciplina: ");
            String nomeDisciplina = scanner.nextLine();

            List<Disciplina> disciplinasEncontradas = new ArrayList<>();
            for (Aluno aluno : alunos) {
                List<Disciplina> disciplinasMatriculadas = aluno.getDisciplinasMatriculadas();
                for (Disciplina disciplina : disciplinasMatriculadas) {
                    if (disciplina.getNomeDisciplina().equalsIgnoreCase(nomeDisciplina)) {
                        disciplinasEncontradas.add(disciplina);
                    }
                }
            }

            if (disciplinasEncontradas.isEmpty()) {
                throw new Excecao.DisciplinaNaoExisteException();
            }

            Disciplina disciplinaRemover = null;
            if (disciplinasEncontradas.size() > 1) {
                System.out.print("Código da Disciplina: ");
                String codigoDisciplina = scanner.nextLine();

                for (Disciplina disciplina : disciplinasEncontradas) {
                    if (disciplina.getCodigoDisciplina().equalsIgnoreCase(codigoDisciplina)) {
                        disciplinaRemover = disciplina;
                        break;
                    }
                }

                if (disciplinaRemover == null) {
                    throw new Excecao.DisciplinaNaoExisteException();
                }
            } else {
                disciplinaRemover = disciplinasEncontradas.get(0);
            }

            List<Aluno> alunosMatriculados = disciplinaRemover.getAlunosMatriculados();
            System.out.println("Alunos Matriculados na Disciplina:");
            for (Aluno aluno : alunosMatriculados) {
                System.out.println(aluno);
            }
            System.out.println();

            System.out.print("Nome do Aluno: ");
            String nomeAluno = scanner.nextLine();

            List<Aluno> alunosEncontrados = new ArrayList<>();
            for (Aluno aluno : alunosMatriculados) {
                if (aluno.getNome().equalsIgnoreCase(nomeAluno)) {
                    alunosEncontrados.add(aluno);
                }
            }

            if (alunosEncontrados.isEmpty()) {
                System.out.println("Aluno não encontrado na disciplina.");
                return;
            }

            Aluno alunoRemover = null;
            if (alunosEncontrados.size() > 1) {
                System.out.print("RG do Aluno: ");
                String rgAluno = scanner.nextLine();

                for (Aluno aluno : alunosEncontrados) {
                    if (aluno.getRg().equalsIgnoreCase(rgAluno)) {
                        alunoRemover = aluno;
                        break;
                    }
                }

                if (alunoRemover == null) {
                    System.out.println("Aluno não encontrado na disciplina.");
                    return;
                }
            } else {
                alunoRemover = alunosEncontrados.get(0);
            }

            alunoRemover.removerDisciplina(disciplinaRemover);
            disciplinaRemover.removerAluno(alunoRemover);

            System.out.println("Matrícula removida com sucesso!");
            System.out.println();

            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");

            String sql = "DELETE FROM Matriculas WHERE id_aluno = ? AND id_disciplina = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, alunoRemover.getAlunoId());
            statement.setString(2, disciplinaRemover.getDisciplinaId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    public static void alterarProfessorDisciplina(List<Professor> professores, List<Disciplina> disciplinas) throws Excecao.DisciplinaNaoEncontradaException {
        Scanner scanner = new Scanner(System.in);
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            System.out.print("Nome do Professor: ");
            String nomeProfessor = scanner.nextLine();

            List<Professor> professoresEncontrados = new ArrayList<>();
            for (Professor professor : professores) {
                if (professor.getNome().equalsIgnoreCase(nomeProfessor)) {
                    professoresEncontrados.add(professor);
                }
            }

            if (professoresEncontrados.isEmpty()) {
                System.out.println("Professor não encontrado.");
                return;
            }

            Professor professorEncontrado = null;
            if (professoresEncontrados.size() > 1) {
                System.out.print("CPF do Professor: ");
                String cpfProfessor = scanner.nextLine();

                for (Professor professor : professoresEncontrados) {
                    if (professor.getCpf().equalsIgnoreCase(cpfProfessor)) {
                        professorEncontrado = professor;
                        break;
                    }
                }

                if (professorEncontrado == null) {
                    System.out.println("Professor não encontrado.");
                    return;
                }

                System.out.println("Professor encontrado:");
                System.out.println(professorEncontrado);
                System.out.println();
            } else {
                professorEncontrado = professoresEncontrados.get(0);
                System.out.println("Professor encontrado:");
                System.out.println(professorEncontrado);
                System.out.println();
            }

            System.out.print("Nome da Disciplina: ");
            String nomeDisciplina = scanner.nextLine();

            List<Disciplina> disciplinasEncontradas = new ArrayList<>();
            for (Disciplina disciplina : disciplinas) {
                if (disciplina.getNomeDisciplina().equalsIgnoreCase(nomeDisciplina)) {
                    disciplinasEncontradas.add(disciplina);
                }
            }

            if (disciplinasEncontradas.isEmpty()) {
                throw new Excecao.DisciplinaNaoEncontradaException("Disciplina não encontrada.");
            }

            Disciplina disciplinaEncontrada = null;
            if (disciplinasEncontradas.size() > 1) {
                System.out.print("Código da Disciplina: ");
                String codigoDisciplina = scanner.nextLine();

                for (Disciplina disciplina : disciplinasEncontradas) {
                    if (disciplina.getCodigoDisciplina().equalsIgnoreCase(codigoDisciplina)) {
                        disciplinaEncontrada = disciplina;
                        break;
                    }
                }

                if (disciplinaEncontrada == null) {
                    throw new Excecao.DisciplinaNaoEncontradaException("Disciplina não encontrada.");
                }

                System.out.println("Disciplina encontrada:");
                System.out.println(disciplinaEncontrada);
                System.out.println();
            } else {
                disciplinaEncontrada = disciplinasEncontradas.get(0);
                System.out.println("Disciplina encontrada:");
                System.out.println(disciplinaEncontrada);
                System.out.println();
            }

            disciplinaEncontrada.setProfessorResponsavel(professorEncontrado);

            System.out.println("Professor da disciplina alterado com sucesso!");
            System.out.println();

            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");

            String sql = "UPDATE Disciplinas SET id_professor = ? WHERE id_disciplina = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, professorEncontrado.getProfessorId());
            statement.setString(2, disciplinaEncontrada.getDisciplinaId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
