package exercise2;

public class Professor {
    private String id;

    private String cpf;
    private String nome;
    private String titulacao;

    public Professor(String id, String cpf, String nome, String titulacao) {
        this.id = id;
        this.cpf = cpf;
        this.nome = nome;
        this.titulacao = titulacao;
    }

    // Getters e Setters
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTitulacao() {
        return titulacao;
    }

    public void setTitulacao(String titulacao) {
        this.titulacao = titulacao;
    }
    public String getProfessorId() {
        return id;
    }
}
