package exercise2;

import java.util.List;
import java.util.Scanner;

import static exercise2.Cadastro.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opcao;
        do {
            System.out.println("Escolha uma opção:");
            System.out.println("1 - Cadastrar Aluno");
            System.out.println("2 - Cadastrar Curso");
            System.out.println("3 - Cadastrar Disciplina");
            System.out.println("4 - Cadastrar Professor");
            System.out.println("5 - Alterar curso do Aluno");
            System.out.println("6 - Matricular aluno em uma Disciplina");
            System.out.println("7 - Remover aluno de uma Disciplina");
            System.out.println("8 - Alterar Professor de uma Disciplina");
            System.out.println("A - Consultar todos os alunos cadastrados");
            System.out.println("B - Consultar alunos associados a um curso");
            System.out.println("C - Consultar disciplinas matriculadas por um aluno");
            System.out.println("D - Consultar ranking de disciplinas pelo número de alunos matriculados");
            System.out.println("E - Consultar um aluno específico");
            System.out.println("0 - Sair");

            String opcaoStr = scanner.nextLine();
            opcao = Character.toUpperCase(opcaoStr.charAt(0));

            switch (opcao) {
                case '1' -> {
                    Aluno aluno = cadastrarAluno();
                    System.out.println("Aluno cadastrado com sucesso:");
                    System.out.println(aluno);
                    System.out.println();
                }
                case '2' -> {
                    try {
                        Curso curso = cadastrarCurso();
                        System.out.println("Curso cadastrado com sucesso:");
                        System.out.println(curso);
                        System.out.println();
                    } catch (Excecao.CadastroCursoException e) {
                        System.out.println("Erro ao cadastrar o curso: " + e.getMessage());
                        System.out.println();
                    }
                }
                case '3' -> {
                    try {
                        Disciplina disciplina = cadastrarDisciplina();
                        System.out.println("Disciplina cadastrada com sucesso:");
                        System.out.println(disciplina);
                        System.out.println();
                    } catch (Excecao.CadastroDisciplinaException e) {
                        System.out.println("Erro ao cadastrar a disciplina: " + e.getMessage());
                        System.out.println();
                    }
                }
                case '4' -> {
                    try {
                        Professor professor = cadastrarProfessor();
                        System.out.println("Professor cadastrado com sucesso:");
                        System.out.println(professor);
                        System.out.println();
                    } catch (Excecao.CadastroProfessorException e) {
                        System.out.println("Erro ao cadastrar o professor: " + e.getMessage());
                        System.out.println();
                    }
                }
                case '5' -> {
                    try {
                        alterarCursoAluno();
                        System.out.println("Curso do aluno alterado com sucesso.");
                        System.out.println();
                    } catch (Excecao.CursoNaoCadastradoException e) {
                        System.out.println("O curso informado não está cadastrado.");
                        System.out.println();
                    } catch (Excecao.AlterarCursoAlunoException e) {
                        System.out.println("Erro ao alterar o curso do aluno: " + e.getMessage());
                        System.out.println();
                    }
                }
                case '6' -> {
                    try {
                        List<Aluno> alunosCadastrados = Cadastro.getAlunos();
                        List<Disciplina> disciplinasCadastradas = Cadastro.getDisciplinas();
                        matricularAlunoEmDisciplina(alunosCadastrados, disciplinasCadastradas);
                        System.out.println("Aluno matriculado na disciplina com sucesso!");
                        System.out.println();
                    } catch (Excecao.MatriculaAlunoDisciplinaException e) {
                        System.out.println("Erro ao realizar a matrícula do aluno na disciplina: " + e.getMessage());
                        System.out.println();
                    }
                }
                case '7' -> {
                    try {
                        List<Aluno> alunosCadastrados = Cadastro.getAlunos();
                        removerMatriculaDisciplina(alunosCadastrados);
                        System.out.println("Matrícula removida com sucesso!");
                        System.out.println();
                    } catch (Excecao.DisciplinaNaoExisteException e) {
                        System.out.println("Erro ao remover a matrícula do aluno na disciplina: Disciplina não existe.");
                        System.out.println();
                    }
                }
                case '8' -> {
                    try {
                        List<Professor> professoresCadastrados = Cadastro.getProfessores();
                        List<Disciplina> disciplinasCadastradas = Cadastro.getDisciplinas();
                        alterarProfessorDisciplina(professoresCadastrados, disciplinasCadastradas);
                        System.out.println("Professor da disciplina alterado com sucesso!");
                        System.out.println();
                    } catch (Excecao.DisciplinaNaoEncontradaException e) {
                        System.out.println("Erro ao alterar o professor da disciplina: " + e.getMessage());
                        System.out.println();
                    }
                }
                case 'A' -> {
                    System.out.println("Consulta de Alunos Cadastrados:");
                    System.out.println();
                    Consulta.lerAlunosCadastrados();
                    System.out.println();
                }
                case 'B' -> {
                    System.out.println("Consulta de Alunos Associados a um Curso:");
                    System.out.println("Digite o ID do curso:");
                    String cursoId = scanner.nextLine();
                    System.out.println();
                    Consulta.consultarAlunosPorCurso(cursoId);
                    System.out.println();
                }
                case 'C' -> {
                    System.out.println("Consulta de Disciplinas Matriculadas por um Aluno:");
                    System.out.println("Digite o ID do aluno:");
                    String alunoId = scanner.nextLine();
                    System.out.println();
                    Consulta.consultarAlunosPorDisciplina(alunoId);
                    System.out.println();
                }
                case 'D' -> {
                    System.out.println("Consulta de Ranking de Disciplinas pelo Número de Alunos Matriculados:");
                    System.out.println();
                    Consulta.consultarRankingDisciplinas();
                    System.out.println();
                }
                case 'E' -> {
                    System.out.println("Consulta de Aluno:");
                    System.out.println("Digite o ID do aluno:");
                    String alunoId = scanner.nextLine();
                    System.out.println();
                    Consulta.consultarAluno(alunoId);
                    System.out.println();
                }

                case '0' -> System.out.println("Encerrando o programa...");
                default -> System.out.println("Opção inválida. Por favor, escolha novamente.");
            }
            System.out.println();
        } while (opcao != '0');
    }
}
