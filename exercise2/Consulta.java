package exercise2;

import java.sql.*;

public class Consulta {
    public static void lerAlunosCadastrados() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");
            statement = connection.createStatement();

            String query = "SELECT Alunos.*, Cursos.nome_curso " +
                    "FROM Alunos " +
                    "INNER JOIN Cursos ON Alunos.id_curso = Cursos.id_curso";

            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                String alunoId = resultSet.getString("aluno_id");
                String nome = resultSet.getString("nome");
                String rg = resultSet.getString("rg");
                String curso = resultSet.getString("nome_curso");

                System.out.println("Aluno ID: " + alunoId);
                System.out.println("Nome: " + nome);
                System.out.println("RG: " + rg);
                System.out.println("Curso: " + curso);
                System.out.println();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void consultarAluno(String alunoId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");

            String query = "SELECT Alunos.*, Cursos.nome_curso, Disciplinas.nome_disciplina " +
                    "FROM Alunos " +
                    "INNER JOIN Cursos ON Alunos.id_curso = Cursos.id_curso " +
                    "INNER JOIN Matriculas ON Alunos.aluno_id = Matriculas.id_aluno " +
                    "INNER JOIN Disciplinas ON Matriculas.id_disciplina = Disciplinas.id_disciplina " +
                    "WHERE Alunos.aluno_id = ?";

            statement = connection.prepareStatement(query);
            statement.setString(1, alunoId);

            resultSet = statement.executeQuery();

            boolean alunoEncontrado = false;

            while (resultSet.next()) {
                if (!alunoEncontrado) {
                    String nome = resultSet.getString("nome");
                    String rg = resultSet.getString("rg");
                    String curso = resultSet.getString("nome_curso");

                    System.out.println("Aluno ID: " + alunoId);
                    System.out.println("Nome: " + nome);
                    System.out.println("RG: " + rg);
                    System.out.println("Curso: " + curso);
                    System.out.println();

                    alunoEncontrado = true;
                }

                String disciplina = resultSet.getString("nome_disciplina");
                System.out.println("Disciplina Matriculada: " + disciplina);
            }

            if (!alunoEncontrado) {
                System.out.println("Aluno não encontrado.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void consultarAlunosPorCurso(String cursoId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");

            String query = "SELECT Alunos.* " +
                    "FROM Alunos " +
                    "INNER JOIN Cursos ON Alunos.id_curso = Cursos.id_curso " +
                    "WHERE Cursos.id_curso = ?";

            statement = connection.prepareStatement(query);
            statement.setString(1, cursoId);

            resultSet = statement.executeQuery();

            boolean alunosEncontrados = false;

            while (resultSet.next()) {
                if (!alunosEncontrados) {
                    System.out.println("Alunos do Curso " + cursoId + ":");
                    System.out.println();
                    alunosEncontrados = true;
                }

                String alunoId = resultSet.getString("aluno_id");
                String nome = resultSet.getString("nome");
                String rg = resultSet.getString("rg");

                System.out.println("Aluno ID: " + alunoId);
                System.out.println("Nome: " + nome);
                System.out.println("RG: " + rg);
                System.out.println();
            }

            if (!alunosEncontrados) {
                System.out.println("Nenhum aluno encontrado para o curso " + cursoId + ".");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void consultarAlunosPorDisciplina(String disciplinaId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");

            String query = "SELECT Alunos.* " +
                    "FROM Alunos " +
                    "INNER JOIN Matriculas ON Alunos.aluno_id = Matriculas.id_aluno " +
                    "WHERE Matriculas.id_disciplina = ?";

            statement = connection.prepareStatement(query);
            statement.setString(1, disciplinaId);

            resultSet = statement.executeQuery();

            boolean alunosEncontrados = false;

            while (resultSet.next()) {
                if (!alunosEncontrados) {
                    System.out.println("Alunos Matriculados na Disciplina " + disciplinaId + ":");
                    System.out.println();
                    alunosEncontrados = true;
                }

                String alunoId = resultSet.getString("aluno_id");
                String nome = resultSet.getString("nome");
                String rg = resultSet.getString("rg");

                System.out.println("Aluno ID: " + alunoId);
                System.out.println("Nome: " + nome);
                System.out.println("RG: " + rg);
                System.out.println();
            }

            if (!alunosEncontrados) {
                System.out.println("Nenhum aluno encontrado matriculado na disciplina " + disciplinaId + ".");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void consultarRankingDisciplinas() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\mauri\\IdeaProjects\\exercises\\exercise2\\db\\banco.db");

            String query = "SELECT Disciplinas.*, COUNT(Matriculas.id_aluno) AS num_alunos " +
                    "FROM Disciplinas " +
                    "LEFT JOIN Matriculas ON Disciplinas.disciplina_id = Matriculas.id_disciplina " +
                    "GROUP BY Disciplinas.disciplina_id " +
                    "ORDER BY num_alunos DESC";

            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            boolean disciplinasEncontradas = false;
            int ranking = 1;

            while (resultSet.next()) {
                if (!disciplinasEncontradas) {
                    System.out.println("Ranking de Disciplinas por Número de Alunos Matriculados:");
                    System.out.println();
                    disciplinasEncontradas = true;
                }

                String disciplinaId = resultSet.getString("disciplina_id");
                String nomeDisciplina = resultSet.getString("nome_disciplina");
                int numAlunos = resultSet.getInt("num_alunos");

                System.out.println("Ranking: " + ranking);
                System.out.println("Disciplina ID: " + disciplinaId);
                System.out.println("Nome da Disciplina: " + nomeDisciplina);
                System.out.println("Número de Alunos Matriculados: " + numAlunos);
                System.out.println();

                ranking++;
            }

            if (!disciplinasEncontradas) {
                System.out.println("Nenhuma disciplina encontrada.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

