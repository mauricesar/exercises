package exercise2;

import java.util.List;

public class Disciplina {
    private String id;
    private String nomeDisciplina;
    private String codigoDisciplina;
    private String descricao;
    private Professor professorResponsavel;
    private List<Aluno> alunosMatriculados;

    public Disciplina(String id, String nomeDisciplina, String codigoDisciplina, String descricao) {
        this.id = id;
        this.nomeDisciplina = nomeDisciplina;
        this.codigoDisciplina = codigoDisciplina;
        this.descricao = descricao;
    }

    // Getters e Setters
    public String getNomeDisciplina() {
        return nomeDisciplina;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
        this.nomeDisciplina = nomeDisciplina;
    }

    public String getCodigoDisciplina() {
        return codigoDisciplina;
    }

    public void setCodigoDisciplina(String codigoDisciplina) {
        this.codigoDisciplina = codigoDisciplina;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Professor getProfessorResponsavel() {
        return professorResponsavel;
    }

    public void setProfessorResponsavel(Professor professorResponsavel) {
        this.professorResponsavel = professorResponsavel;
    }

    public void adicionarAluno(Aluno aluno) {
        alunosMatriculados.add(aluno);
    }

    public List<Aluno> getAlunosMatriculados() {
        return alunosMatriculados;
    }

    public void removerAluno(Aluno aluno) {
        alunosMatriculados.remove(aluno);
    }
    public String getDisciplinaId() {
        return id;
    }

}
