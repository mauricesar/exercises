package exercise2;

import java.time.LocalDate;

public class Curso {
    private String id;
    private String nome;
    private LocalDate dataCriacao;
    private String nomePredio;
    private Professor coordenador;

    public Curso(String id, String nome, LocalDate dataCriacao, String nomePredio) {
        this.id = id;
        this.nome = nome;
        this.dataCriacao = dataCriacao;
        this.nomePredio = nomePredio;
    }

    // Getters e Setters

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(LocalDate dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public String getNomePredio() {
        return nomePredio;
    }

    public void setNomePredio(String nomePredio) {
        this.nomePredio = nomePredio;
    }

    public Professor getCoordenador() {
        return coordenador;
    }

    public void setCoordenador(Professor coordenador) {
        this.coordenador = coordenador;
    }
    public String getId() {
        return id;
    }
}
