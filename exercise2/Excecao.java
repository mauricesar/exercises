package exercise2;

public class Excecao {

    public static class CursoNaoCadastradoException extends Exception {
        public CursoNaoCadastradoException() {
            super("Curso não cadastrado no sistema.");
        }
    }

    public static class DisciplinaNaoCadastradaException extends Exception {
        public DisciplinaNaoCadastradaException() {
            super("Essa disciplina não encontra-se cadastrada.");
        }
    }

    public static class DisciplinaNaoExisteException extends Exception {
        public DisciplinaNaoExisteException() {
            super("Essa disciplina não existe, logo, não será possível remover.");
        }
    }

    public static class DisciplinaNaoEncontradaException extends Exception {
        public DisciplinaNaoEncontradaException(String message) {
            super();
        }
    }

    public class AlunoNaoEncontradoException extends Exception {
        public AlunoNaoEncontradoException(String message) {
            super("Aluno não encontrado: " + message);
        }
    }

    public static class CadastroCursoException extends Exception {
        public CadastroCursoException(String message) {
            super("Erro ao cadastrar o curso: " + message);
        }
    }

    public static class CadastroDisciplinaException extends Exception {
        public CadastroDisciplinaException(String message) {
            super("Erro ao cadastrar disciplina: " + message);
        }
    }

    public static class CadastroProfessorException extends Exception {
        public CadastroProfessorException(String message) {
            super("Erro ao cadastrar o professor: " + message);
        }
    }

    public static class AlterarCursoAlunoException extends Exception {
        public AlterarCursoAlunoException(String message) {
            super("Erro ao alterar o curso do aluno. " + message);
        }
    }

    public static class MatriculaAlunoDisciplinaException extends Exception {
        public MatriculaAlunoDisciplinaException(String message) {
            super("Erro ao realizar a matrícula do aluno na disciplina: " + message);
        }
    }


}
