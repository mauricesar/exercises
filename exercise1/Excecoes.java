package exercise1;

public class Excecoes {
    public static class ProdutoDuplicadoException extends Exception {
        public ProdutoDuplicadoException(String nome) {
            super("Já existe um produto com o nome '" + nome + "'. Cadastro não realizado.");
        }
    }

    public static class ValorNegativoException extends Exception {
        public ValorNegativoException(String campo) {
            super("O valor do campo '" + campo + "' não pode ser negativo.");
        }
    }

    public static class ProdutoNaoEncontradoException extends Exception {
        public ProdutoNaoEncontradoException(String nome) {
            super("exercise1.Produto '" + nome + "' não encontrado.");
        }
    }

    public static class ValorInvalidoException extends Exception {
        public ValorInvalidoException(String campo, String tipoEsperado) {
            super("Valor inválido inserido para o campo '" + campo + "'. Tipo esperado: " + tipoEsperado);
        }
    }

    public static class ValorEstoqueInvalidoException extends ValorInvalidoException {
        public ValorEstoqueInvalidoException(String campo) {
            super(campo, "inteiro");
        }
    }

    public static class ValorPrecoInvalidoException extends ValorInvalidoException {
        public ValorPrecoInvalidoException(String campo) {
            super(campo, "inteiro ou double");
        }
    }

}
