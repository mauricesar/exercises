package exercise1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        CadastroProdutos cadastro = new CadastroProdutos();
        Scanner scanner = new Scanner(System.in);

        boolean executar = true;
        while (executar) {
            System.out.println("======= MENU =======");
            System.out.println("1 - Cadastrar produto");
            System.out.println("2 - Procurar produto por nome");
            System.out.println("3 - Atualizar produto por nome");
            System.out.println("4 - Remover produto por nome");
            System.out.println("5 - Verificar estoque por nome");
            System.out.println("6 - Sair");
            System.out.print("Escolha uma opção: ");

            int opcao = scanner.nextInt();
            scanner.nextLine();

            switch (opcao) {
                case 1:
                    System.out.println("\n--- Cadastro de exercise1.Produto ---");
                    cadastro.adicionarProduto();
                    System.out.println("--------------------------\n");
                    break;
                case 2:
                    System.out.println("\n--- Procurar exercise1.Produto por Nome ---");
                    System.out.print("Digite o nome do produto: ");
                    String nomeProduto = scanner.nextLine();
                    cadastro.procurarProdutoPorNome(nomeProduto);
                    System.out.println("--------------------------\n");
                    break;
                case 3:
                    System.out.println("\n--- Atualizar exercise1.Produto por Nome ---");
                    System.out.print("Digite o nome do produto: ");
                    String nomeProdutoAtualizacao = scanner.nextLine();
                    cadastro.atualizarProdutoPorNome(nomeProdutoAtualizacao);
                    System.out.println("--------------------------\n");
                    break;
                case 4:
                    System.out.println("\n--- Remover exercise1.Produto por Nome ---");
                    System.out.print("Digite o nome do produto: ");
                    String nomeProdutoRemocao = scanner.nextLine();
                    cadastro.removerProdutoPorNome(nomeProdutoRemocao);
                    System.out.println("--------------------------\n");
                    break;
                case 5:
                    System.out.println("\n--- Verificar Estoque por Nome ---");
                    System.out.print("Digite o nome do produto: ");
                    String nomeProdutoVerificacao = scanner.nextLine();
                    try {
                        cadastro.verificarEstoquePorNome(nomeProdutoVerificacao);
                    } catch (Excecoes.ProdutoNaoEncontradoException e) {
                        System.out.println(e.getMessage());
                    }
                    System.out.println("--------------------------\n");
                    break;
                case 6:
                    System.out.println("Encerrando o programa...");
                    executar = false;
                    break;
                default:
                    System.out.println("Opção inválida!");
                    break;
            }
        }

        scanner.close();
    }
}
