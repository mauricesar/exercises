package exercise1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;


public class CadastroProdutos {
    private List<Produto> produtos;
    private Scanner scanner;

    public CadastroProdutos() {
        produtos = new ArrayList<>();
        scanner = new Scanner(System.in);
    }

    public void adicionarProduto() throws Excecoes.ProdutoDuplicadoException,
            Excecoes.ValorNegativoException, Excecoes.ValorPrecoInvalidoException,
            Excecoes.ValorEstoqueInvalidoException {
        System.out.print("Digite o nome do produto: ");
        String nome = scanner.nextLine();

        if (existeProdutoComNome(nome)) {
            throw new Excecoes.ProdutoDuplicadoException(nome);
        }

        System.out.print("Digite a descrição do produto: ");
        String descricao = scanner.nextLine();

        System.out.print("Digite o preço do produto: ");
        String precoInput = scanner.nextLine();
        double preco;
        try {
            preco = Double.parseDouble(precoInput);
            if (preco < 0) {
                throw new Excecoes.ValorNegativoException("preço");
            }
        } catch (NumberFormatException e) {
            throw new Excecoes.ValorPrecoInvalidoException("preço");
        }

        System.out.print("Digite a quantidade em estoque do produto: ");
        String quantidadeEstoqueInput = scanner.nextLine();
        int quantidadeEstoque;
        try {
            quantidadeEstoque = Integer.parseInt(quantidadeEstoqueInput);
            if (quantidadeEstoque < 0) {
                throw new Excecoes.ValorNegativoException("quantidade em estoque");
            }
        } catch (NumberFormatException e) {
            throw new Excecoes.ValorEstoqueInvalidoException("quantidade em estoque");
        }

        String id = UUID.randomUUID().toString(); // Gerar um id único e aleatório

        Produto produto = new Produto(id, nome, descricao, preco, quantidadeEstoque);
        produtos.add(produto);

        System.out.println("Produto adicionado com sucesso: " + produto.getNome());
        System.out.println("ID do produto: " + produto.getId());
    }



    private boolean existeProdutoComNome(String nome) {
        for (Produto produto : produtos) {
            if (produto.getNome().equalsIgnoreCase(nome)) {
                return true;
            }
        }
        return false;
    }


    public Produto procurarProdutoPorNome(String nome) throws Excecoes.ProdutoNaoEncontradoException {
        for (Produto produto : produtos) {
            if (produto.getNome().equalsIgnoreCase(nome)) {
                System.out.println("exercise1.Produto encontrado: " + produto.getNome());
                System.out.println("ID: " + produto.getId());
                System.out.println("Descrição: " + produto.getDescricao());
                System.out.println("Preço: " + produto.getPreco());
                System.out.println("Quantidade em estoque: " + produto.getQuantidadeEstoque());
                return produto;
            }
        }
        throw new Excecoes.ProdutoNaoEncontradoException(nome);
    }



    public void atualizarProdutoPorNome(String nome) throws Excecoes.ValorNegativoException, Excecoes.ProdutoNaoEncontradoException {
        Produto produto = procurarProdutoPorNome(nome);

        if (produto != null) {
            System.out.println("exercise1.Produto encontrado: " + produto.getNome());
            System.out.println("Selecione o campo a ser atualizado:");
            System.out.println("1 - Descrição");
            System.out.println("2 - Preço");
            System.out.println("3 - Quantidade em estoque");
            System.out.println("0 - Cancelar");

            int opcao = scanner.nextInt();
            scanner.nextLine(); // Consumir a quebra de linha deixada pelo nextInt()

            switch (opcao) {
                case 1 -> {
                    System.out.print("Digite a nova descrição: ");
                    String novaDescricao = scanner.nextLine();
                    produto.setDescricao(novaDescricao);
                    System.out.println("Descrição atualizada com sucesso.");
                }
                case 2 -> {
                    System.out.print("Digite o novo preço: ");
                    double novoPreco = scanner.nextDouble();
                    if (novoPreco < 0) {
                        throw new Excecoes.ValorNegativoException("O preço não pode ser negativo.");
                    }
                    produto.setPreco(novoPreco);
                    System.out.println("Preço atualizado com sucesso.");
                }
                case 3 -> {
                    System.out.print("Digite a nova quantidade em estoque: ");
                    int novaQuantidade = scanner.nextInt();
                    if (novaQuantidade < 0) {
                        throw new Excecoes.ValorNegativoException("A quantidade em estoque não pode ser negativa.");
                    }
                    produto.setQuantidadeEstoque(novaQuantidade);
                    System.out.println("Quantidade em estoque atualizada com sucesso.");
                }
                case 0 -> System.out.println("Operação cancelada.");
                default -> System.out.println("Opção inválida!");
            }
        } else {
            System.out.println("exercise1.Produto não encontrado.");
        }
    }

    public void removerProdutoPorNome(String nome) throws Excecoes.ProdutoNaoEncontradoException {
        Produto produto = procurarProdutoPorNome(nome);

        if (produto != null) {
            produtos.remove(produto);
            System.out.println("exercise1.Produto removido com sucesso: " + produto.getNome());
        } else {
            System.out.println("exercise1.Produto não encontrado.");
        }
    }

    public void verificarEstoquePorNome(String nome) throws Excecoes.ProdutoNaoEncontradoException {
        Produto produto = procurarProdutoPorNome(nome);

        if (produto != null) {
            int quantidadeEstoque = produto.getQuantidadeEstoque();
            System.out.println("Estoque do produto '" + nome + "': " + quantidadeEstoque);
        } else {
            throw new Excecoes.ProdutoNaoEncontradoException(nome);
        }
    }
}
